# -*- coding: utf-8 -*-
{
    'name': "Cars Fleet",

    'summary': """
        Car's Fleet control for the Persiscal job interview    
    """,

    'description': """
        This module was developed as a jow interview test for Persiscal
    """,

    'author': "Marcos Talento",
    'website': "https://www.linkedin.com/in/marcos-talento-88286b48/",

    'category': 'Human Resources',
    'version': '0.1',

    'depends': ['base'],

    'data': [
        'report/fleet_assets_report_template.xml',
        'report/fleet_assets_report.xml',

        'wizard/fleet_report_wizard_view.xml',

        'views/car_brands_view.xml',
        'views/car_models_view.xml',
        'views/car_view.xml',
        'views/menu.xml',

        'security/ir.model.access.csv',
    ],
    'demo': [
        'demo/demo.xml',
    ],
    'auto_install': True,
    'application': True,
}
